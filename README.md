# Xataface Tutoriel

## contenu et objectifs

Ce tutoriel se veut un kit de démarrage :

  * pour découvrir le Framework Xataface
  * pour se repérer dans la dcoumentation officielle

Ce tutoriel est rédigé au format Sphinx (ReStructuredText).


## installation de l'environnement Sphinx

  * sphinx <http://sphinx-doc.org/>


```
# dépendance spécifique Debian/Ubuntu
$ sudo apt install python3-venv
# environnement virtuel Python
$ python3 -m venv .venv
$ source .venv/bin/activate
(.venv) $ python3 -m pip install pip -U
(.venv) $ python3 -m pip install sphinx -U
(.venv) $ python3 -m pip install piccolo_theme -U
```


## compilation de la documentation (format HTML)


```
$ (.venv) make html
```


## localisation de la documentation

  * xatatuto/build/html/index.html


## contribuer au projet Xataface

  * forum officiel Xataface

    [https://groups.google.com/forum/#!forum/xataface](https://groups.google.com/forum/#!forum/xataface)

  * dépôt Github du projet Xataface

    [https://github.com/shannah/xataface](https://github.com/shannah/xataface)
