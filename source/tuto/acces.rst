gérer les accès (~ 10 min) / à compléter
=========================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------

.. tip:: Ouvrez un terminal et déplacez-vous jusqu'au répertoire de votre serveur Web

         WEBSITE=$PWD


utilisateurs anonymes
----------------------


module switchUser
------------------

Ajoutez le module dans le fichier *conf.ini*

.. code-block:: ini

    ;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -| MODULES
    
    [_modules]
    modules_switch_user = modules/switch_user/switch_user.php


.. code-block:: php

    <?php
    class conf_ApplicationDelegate {
    
        // ...
    
        /**
         * @brief Method to determine if the currently logged in user is allowed to switch users.
         * @return boolean True if the user is allowed to switch accounts. False otherwise.
         */
        function canSwitchUser(){
            $auth =& Dataface_AuthenticationTool::getInstance();
            $user =& $auth->getLoggedInUser();
            if( ! isset($user) ) return false;
            else {
                $role = $user->val('role');
                return $role  === 'ADMIN';
            }
        }


activation demande création de compte
--------------------------------------


filtrer des données pour tous utilisateurs
--------------------------------------------

Ajoutez une section *[__filters__]* dans le fichier *fields.ini* de la table que vous souhaitez filtrer

.. code-block:: ini

    [__filters__]
    annee > 2015


.. note:: Le filtre s'applique à tous les utilisateurs du système, y compris les administrateurs.


filtre spécifique à l'utilisateur/propriétaire
------------------------------------------------

principes
~~~~~~~~~~

restreindre les permissions en fonction du rôle de l'utilisateur authentifié


extrait
~~~~~~~~

conf/ApplicationDelegate.php

.. code-block:: php

    <?php
    class conf_ApplicationDelegate {
    
        function getPreferences(){
            $table =& Dataface_Table::loadTable('nomTable')

            $auth =& Dataface_AuthenticationTool::getInstance();
            $user =& $auth->getLoggedInUser();

            if ( $user && $user->val('role') != 'ADMIN' ){
                $tab->setSecurityFilter(array('owner_id'=>$user->val('id')));
            }
        return array();

    }



verrou lors de l'édition d'un enregistrement
----------------------------------------------

* https://groups.google.com/d/msgid/xataface/6c17c546-4099-4796-8de9-ad44508fa62f%40googlegroups.com?utm_medium=email&utm_source=footer
