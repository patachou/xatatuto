paramétrage de confort (~ 10 min)
==================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------

.. tip:: Ouvrez un terminal et déplacez-vous jusqu'au répertoire de votre serveur Web

         WEBSITE=$PWD


internationalisation
---------------------

activation
~~~~~~~~~~~

Ajoutez dans votre fichier *conf.ini* **entre** [__database] et [_tables]

.. code-block:: ini

    ;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -| TRADUCTIONS

    ;; si non défini, l'application s'ouvrira
    ;; dans la 1ère langue définie dans [languages]
    default_language = fr

    [languages]
    fr = "Français"
    en = "Anglais"


fichiers de traductions
~~~~~~~~~~~~~~~~~~~~~~~~

Les fichiers de langues sont situés

* au niveau de Xataface

::

     xataface/ (__DATAFACE_PATH__)
        |
        + lang/
            |
            + fr.ini

        + modules/
            |
            + (pour chaque module installé par défaut)
                       |
                       +lang/
                          |
                          + fr.ini

* au niveau des modules installés spécifiquement pour cette application (cf. tuto modules)

::

     xata_tuto/ (__DATAFACE_SITE_PATH__)
        |
        + modules/
            |
            + (pour chaque module ajouté dans l'application)
                       |
                       +lang/
                          |
                          + fr.ini


* au niveau de votre application (vos propres traductions) dans un fichier *en.ini* ou *fr.ini* selon votre besoin


.. code-block:: bash

    cd $WEBSITE
    cd xata_tuto
    mkdir lang
    cd lang
    touch en.ini

::

     xata_tuto/ (__DATAFACE_SITE_PATH__)
        |
        + lang/
            |
            + en.ini

.. note:: Vous positionnerez les droits en lecture seule pour l'utilisateur *www-data*.

Complétez le fichier *en.ini* avec

.. code-block:: ini

    tables.formation.label = training sessions
    tables.formation.fields.titre.widget.label = Training
    tables.formation.fields.annee.widget.label = Year
    tables.formation.relationships.Participants.label = Members


historique des modifications
-----------------------------

activation 
~~~~~~~~~~~

Ajoutez dans votre fichier *conf.ini*.

.. code-block:: ini

    ;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -| HISTORISATION
    
    [history]
    enabled = 1


comportement
~~~~~~~~~~~~~

* données : pour chaque table de données, ajoute une table *{nomTable}__history*
* fichiers :

  * stockés en base de données : comme pour les données
  * stockés sur disque : ajoute un répertoire caché *.history*

En fonction des droits de l'utilisateur Xataface, un onglet *historique* apparaît pour chaque table pour

- comparer les données entre l'enregistrement courant et une date donnée
- recharger dans l'enregistrement courant les données et fichiers versés à une date antérieure

.. note:: Toute opération s'appuyant sur l'API, y compris le rechargement de données historisées, est également tracée dans l'historique.


ergonomie
----------

onglet historique
~~~~~~~~~~~~~~~~~~

Si vous avez plus de 4 relations avec votre table courante, vous remarquez que l'onglet historique ne se met pas à la fin de la liste des onglets.
L'onglet historique est fixé par défaut à la 4ème position.

Il est possible de surcharger ce comportement : par exemple de positionner l'onglet historique en dernière position.

Créez un fichier *actions.ini*

.. code-block:: bash

    cd $WEBSITE
    cd xata_tuto
    touch actions.ini

.. note:: Vous positionnerez les droits en lecture seule pour l'utilisateur *www-data*.


architecture cible
~~~~~~~~~~~~~~~~~~~

::

    WEBSITE
       |
       + xataface/ = __DATAFACE_PATH__
       + xata_tuto/ = __DATAFACE_SITE_PATH__
              |
              + actions.ini
              + conf.ini
              + .htaccess (fichier caché)
              + index.php
              + tables/
                   |
                   + formation/
                        |
                        + fields.ini
                        + relationships.ini
                        + valuelists.ini
              |
              + template_c/


exemple de surcharge
~~~~~~~~~~~~~~~~~~~~~

Complétez le contenu de *actions.ini*

.. code-block:: ini

    [history > history]
    order=99
    ;; si vous souhaitez par défaut cacher l'onglet historique
    ;; visible = 0


gestion des accès directs aux tables
-------------------------------------

masquer l'accès à la table période
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Reprenez la rubrique [_tables] de votre fichier *conf.ini* et commentez les lignes

* *default_table...*
* *periode = ...*

.. code-block:: ini
   :emphasize-lines: 3,6
    
    ;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - | TABLES

    ;;default_table = formation

    [_tables]
    ;;    periode     = périodes
    formation     = formations

.. warning:: La table *periode* sera toujours accessible en modifiant l'URL ou en ajoutant à la volée une nouvelle période depuis une *formation* -> choix de la période -> *Other...*.

.. note:: Exercice : dans votre navigateur

          - modifiez le paramètre *-table* de l'url et essayez d'accéder directement à la table *annee*
          - créez une nouvelle formation et cliquez sur *other* juste à côté de la liste déroulante des années


nommage pour les tables masquées
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Par défaut, une table non-définie dans *[_tables]* s'affiche avec son nom de table, par exemple : *periode* (et non *périodes* comme souhaité).

Pour modifier ce comportement, reprenez ou créez le fichier *fields.ini* correspondant à cette table

.. code-block:: bash

    cd $WEBSITE
    cd xata_tuto/tables
    mkdir periode
    cd periode
    touch fields.ini

et ajoutez au début

.. code-block:: ini

    label = "périodes"

.. note:: Exercice : dans votre navigateur

          - modifiez le paramètre *-table* de l'url et essayez d'accéder directement à la table *periode*


bloquer l'accès à la table période
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Après *[_tables]*, complétez votre fichier *conf.ini* avec l'extrait suivant

.. code-block:: ini

    [_disallowed_tables]
    r1 = periode

.. note:: Exercice : dans votre navigateur

          - modifiez le paramètre *-table* de l'url et essayez d'accéder directement à la table *periode*
          - créez une nouvelle formation et cliquez sur *other* juste à côté de la liste déroulante des années*


autoriser l'accès aux tables cachées par défaut
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Par défaut, certaines tables systèmes ou liées à des modules sont bloquées. Il est cependant possible d'autoriser explicitement l'accès à ces tables.

Après *[_disallowed_tables]*, complétez votre fichier *conf.ini* avec l'extrait suivant

.. code-block:: ini

    [_allowed_tables]
    r2 = "/__history$/"

L'expression rationnelle permet ici de sélectionner toutes les tables dont le nom se termine par *__history*. 
Vous autorisez ainsi les accès directs à toutes les tables d'historique des données saisies.

.. note:: Exercice : dans votre navigateur

          - modifiez une fiche *formation*
          - consultez l'onglet *historique* de la fiche
          - modifiez le paramètre *-table* de l'url et essayez d'accéder directement à la table *formation__history*.

.. tip:: Les réglages présents dans le fichier *conf.ini* sont généraux.

         La gestion des droits de consultation et de modification des données est présentée dans les chapitres suivants du tuto.


export compatible Excel
------------------------

principes
~~~~~~~~~~

Par défaut, l'export CSV n'est pas immédiatement reconnu par Excel (séparateur, encodage) car Excel ne gère pas très bien l'UTF-8.

Ce comportement est modifiable pour exporter en UTF-16 avec le séparateur tabulation.

activation
~~~~~~~~~~~

Ajoutez une rubrique *[export_csv]* au fichier *conf.ini*

.. code-block:: ini

    ;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - | EXPORT
    
    [export_csv]
    ;; UTF-16 + séparateur tabulation
    format = excel


export orienté utilisateur
~~~~~~~~~~~~~~~~~~~~~~~~~~~

L'export peut reprendre les étiquettes des champs définies dans fields.ini pour les versions

* stables, supérieures à Xataface 2.1.2 (2014)
* de développement, postérieures à mai 2015

Pour cela, complétez la rubrique *[export_csv]* avec

.. code-block:: ini

    heading = label


tests du site
--------------

urls de connexion
~~~~~~~~~~~~~~~~~~

Selon votre configuration du serveur Web

* htdocs/xata_tuto : http://127.0.0.1/xata_tuto
* www/xata_tuto : http://127.0.0.1/xata_tuto
* public_html/xata_tuto :  http://127.0.0.1/~$USER/xata_tuto


références documentaires
-------------------------

*internationalisation*

- http://xataface.com/documentation/tutorial/internationalization-with-dataface-0.6
- http://xataface.com/documentation/how-to/use-translations
- http://xataface.com/documentation/how-to/how-to-internationalize-your-application
- http://xataface.com/documentation/tutorial/internationalization-with-dataface-0.6/internationalizing_field_labels

*historique et autorisation/interdiction des accès tables*

- http://xataface.com/wiki/conf.ini_file
- http://xataface.com/documentation/how-to/history-howto
- http://xataface.com/documentation/how-to/disallow_tables

*export CSV*

- http://xataface-tips.blogspot.fr/2013/09/excel-friendly-csv-files.html
- https://github.com/shannah/xataface/pull/56
