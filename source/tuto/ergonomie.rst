ergonomie (~ 10 min)
=====================

.. contents:: Contenu
   :local:
   :backlinks: top

-------

.. tip:: Ouvrez un terminal et déplacez-vous jusqu'au répertoire de votre serveur Web

         WEBSITE=$PWD


affichage des relations
------------------------

principes
~~~~~~~~~~

En consultant les données de la table *formation*, on remarque des blocs à gauche décrivant la relation *participant* :

de haut en bas

* *record_search* = un champ de recherche
* *record_tree* = pour la relation *participant*, la liste pliée/dépliée des membres
* *section* = pour la relation *participant*, un bloc affiche jusqu'à 5 membres [#f1]_
* *actions* = l'onglet *participants* est positionné avant l'onglet *historique*

Les deux premiers seront abordés dans la configuration générale et le deux derniers, paramétrés relation par relation.

.. rubric:: Notes

.. [#f1] avec indication de la date d'ajout ou dernière modification ; si l'authentification est active, apparaît également le nom de l'utilisateur qui a ajouté/modifié.


réglages généraux
~~~~~~~~~~~~~~~~~~

Pour masquer le champ de recherche et la liste pliée/dépliée, ajoutez cette partie au fichier *conf.ini*

.. code-block:: ini

    ;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -| PRÉFÉRENCES
    
    [_prefs]
    hide_record_search = 1
    show_record_tree = 0

.. warning:: Certains réglages étaient valides pour le thème *Plone* et sont inopérants pour l'actuel thème *G2*.


réglages propres à la relation Participants
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

À l'exception de la position de l'onglet *historique* (des modifications) gérée par le fichier *actions.ini*, par défaut, l'ordre des onglets reprend l'ordre des définitions des relations inscrites dans le fichier *relationships.ini*.

Pour modifier la position de l'onglet *participants*, complétez le fichier *relationships.ini* comme suit

.. code-block:: ini
   :emphasize-lines: 5

    [Participants]
    action:label = Participants
    individu.id_formation = "$id"
    metafields:order = nom ASC, prenom ASC
    action:order = 100
    ;; pour mémo : l'onglet historique est à la position 99

.. note:: Exercice : ajoutez une nouvelle relation 

          - créez une nouvelle table *support* contenant les champs *id*, *ressource* (intitulé *support de formation*) et *id_formation*
          - rajoutez une rubrique *[Supports]* au fichier relationships.ini
          - réarrangez l'ordre des onglets pour obtenir *participants* puis *supports* puis *historique*

Pour afficher jusqu'à 10 participants dans la section *participants*, ajoutez à la suite l'instruction suivante

.. code-block:: ini

    section:limit = 10


ou pour masquer complètement la *section*

.. code-block:: ini

    section:limit = 0


cacher/afficher une propriété de la relation
---------------------------------------------

Depuis *formation*, on affiche la liste des participants mais on souhaite un listing différent de celui par défaut des *individus*.

On souhaite cacher la *civilité*

.. code-block:: ini
   :emphasize-lines: 7

    [Participants]
    action:label = Participants
    individu.id_formation = "$id"
    metafields:order = nom ASC, prenom ASC
    action:order = 100
    ;; pour mémo : l'onglet historique est à la position 99
    visibility:civilite = hidden


nombre d'éléments affichés
---------------------------

principes
~~~~~~~~~~

Quand il y a trop d'éléments dans un bandeau ou dans la barre des onglets, un sous-menu "More..." apparaît.

Il est possible d'augmenter/réduire le nombre d'éléments affichés avant le sous-menu "More...".


configuration générale
~~~~~~~~~~~~~~~~~~~~~~~

Ajoutez une nouvelle rubrique au fichier *conf.ini*

.. code-block:: ini

    ;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -| THÈME G2
    
    [modules_g2]
    ;; bandeau supérieur (à gauche)
    top_left_menu_bar.size = 10
    ;; bandeau supérieur (à droite)
    top_right_menu_bar.size = 5


nombre d'onglets : relations et historique
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ajoutez à la suite les instructions suivantes

.. code-block:: ini

   record_subtabs.size = 12


menu déroulant
---------------

Modifiez le fichier *actions.ini* comme suit

.. code-block:: ini

    ;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -| THÈME G2

    [menu_historique]
    label="programation"
    subcategory=histo_group
    category=top_left_menu_bar
    
    [menu_2016]
    label="2016"
    category=histo_group
    url="{$site_href}?-table=formation&annee=2016"
    
    [menu_2017]
    label="2017"
    category=histo_group
    url="{$site_href}?-table=formation&annee=2017"


organisation des champs
------------------------

ordre des champs
~~~~~~~~~~~~~~~~~

Revenons à la table *formation*.

Modifiez le fichier *fields.ini*

.. code-block:: ini
   :emphasize-lines: 2

    [cloture]
    order = 1
    widget:label = Date de clôture
    validators:regex = "/^\d{4}-\d{2}-\d{2}$/"
    validators:regex:message = "format attendu de la date de clôture = aaaa-mm-jj"
    widget:atts:style = "font-size: 18pt; color: red;"
    widget:description = "au format : aaaa-mm-jj"

.. note:: Modifier l'ordre des champs impacte aussi l'export CSV.


groupe de champs
~~~~~~~~~~~~~~~~~

Complétez le fichier *fields.ini* d'une rubrique *[fieldgroup:xxx]*

.. code-block:: ini

    [fieldgroup:attention]
    label = "Attention !"
    description = "Soyez vigilant sur la saisie de l'année !"

Pour modifier l'ordre des blocs en mode édition, on ajoutera

.. code-block:: ini

    order = 2

pour l'ordre des blocs en mode consultation (section), on ajoutera

.. code-block:: ini

    section:order = 4

et pour afficher sur une même ligne les champs appartenant au même bloc

.. code-block:: ini

   display_style = inline


Pour placer les champs *annee* et *public* dans ce bloc, indiquez

.. code-block:: ini
    :emphasize-lines: 2,7

    [annee]
    group = attention
    widget:label = Année
    widget:atts:maxlength = 4
    
    [public]
    group = attention
    validators:required = true

.. tip:: Le bloc par défaut est *[fieldgroup:__main__]*.

   
onglet pour l'écran de saisie
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

De façon similaire au regroupement par bloc pour l'édition la consultation d'une fiche/édition d'une fiche, il est facile de regrouper pour la saisie plusieurs champs dans un même onglet avec une rubrique *[tab:xxx]*

.. code-block:: ini
    :emphasize-lines: 1,2,3,6

    [tab:info]
    label = "Informations complémentaires"
    order = 2

    [lieu_formation]
    tab = info
    visibility:list = hidden
    visibility:browse = hidden

.. tip:: L'onglet par défaut est *[tab:__main__]*.
    

ressources documentaires
-------------------------

*configuration générale*

- http://xataface.com/wiki/conf.ini_file

*relations*

- http://xataface.com/wiki/relationships.ini_file
- http://xataface.com/forum/viewtopic.php?t=4545
- http://xataface.com/wiki/visibility%3AfieldName

*menu déroulant*

- https://groups.google.com/forum/#!topic/xataface/oZZj-GHlPnM
- http://xataface-tips.blogspot.ca/2013/05/using-category-directive-to-place.html

*regroupement de champs*

- http://xataface.com/wiki/group

*affichage sur une même ligne*

- https://github.com/shannah/xataface/commit/3dd2eacbe53995fa850f6a4ded3d0d2967a11bcc

*saisie sous plusieurs onglets*

- http://xataface.com/wiki/tab

