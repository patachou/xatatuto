paramétrage de base (~ 10 min)
===============================

.. contents:: Contenu
   :local:
   :backlinks: top

-------

.. tip:: Ouvrez un terminal et déplacez-vous jusqu'au répertoire de votre serveur Web

         WEBSITE=$PWD


fichiers de paramètres
-----------------------

préparation
~~~~~~~~~~~~

Le paramétrage de base des écrans de saisie et consultation s'effectue dans une arborescence spécifique avec 3 fichiers par table

* fields.ini
* relationships.ini
* valuelists.ini

Créez l'arborescence pour la table *formation* et les 3 fichiers :

.. code-block:: bash

    table=formation

    cd $WEBSITE
    mkdir -p xata_tuto/tables/$table
    cd xata_tuto/tables/$table
    touch fields.ini relationships.ini valuelists.ini

.. note:: Vous positionnerez les droits en lecture seule pour l'utilisateur *www-data*.


architecture cible
~~~~~~~~~~~~~~~~~~~

::

    WEBSITE
       |
       + xataface/ = __DATAFACE_PATH__
       + xata_tuto/ = __DATAFACE_SITE_PATH__
              |
              + conf.ini
              + .htaccess (fichier caché)
              + index.php
              + tables/
                   |
                   + formation/
                        |
                        + fields.ini
                        + relationships.ini
                        + valuelists.ini
              |
              + template_c/


saisie et consultation
-----------------------

fields.ini
~~~~~~~~~~~

Construisez le fichier *fields.ini* en paramétrant les règles suivantes :

Le champ *id* sera

* caché à l'édition
* caché à la recherche
* caché à l'export CSV

.. code-block:: ini

    [id]
    widget:type = hidden
    visibility:find = hidden
    visibility:csv = hidden

Le champ *formation* sera renommé *titre de la formation*

.. code-block:: ini

    [titre]
    widget:label = Titre de la formation

Le champ *lieu_formation* sera caché dans le listing et à la consultation

.. code-block:: ini

    [lieu_formation]
    visibility:list = hidden
    visibility:browse = hidden

Le champ *annee* sera renommé *Année* et limitée à 4 caractères

.. code-block:: ini

    [annee]
    widget:label = Année
    widget:atts:maxlength = 4

Le champ *public* sera obligatoire
	
.. code-block:: ini

    [public]
    validators:required = true

La saisie de la date de clôture sera

* formatée au format *aaaa-mm-jj*
* affichée en rouge lors de la saisie
* complétée d'une aide pour guider la saisie

.. code-block:: ini

    [cloture]
    widget:label = Date de clôture
    validators:regex = "/^\d{4}-\d{2}-\d{2}$/"
    validators:regex:message = "format attendu de la date de clôture = aaaa-mm-jj"
    widget:atts:style = "font-size: 18pt; color: red;"
    widget:description = "au format : aaaa-mm-jj"

.. note:: Les attributs *atts* --- style, appel fonction javascript --- concernent la saisie.
          
          Un réglage plus fin s'effectuera en modifiant la feuille de style CSS.
          
          Modifier l'affichage des données en consultation est traité au chapitre *ergonomie*.


valuelists.ini
~~~~~~~~~~~~~~~

Ce fichier contient la définition des listes de valeurs :

Complétez le fichier *valuelists.ini* comme suit :

.. code-block:: ini

    [Publics]
    ;; liste statique (courte)
    interne = interne
    externe = externe
    mixte = mixte

    [Années]
    ;; liste dynamique
    __sql__ = "SELECT annee FROM periode ORDER BY annee DESC"

.. note:: L'ordre du résultat de la requête SQL impacte l'écran de recherche.

Revenez au fichier *fields.ini* et ajoutez aux rubriques *[annee]* et *[public]* les paramètres suivants

.. code-block:: ini
   :emphasize-lines: 4,5,9,10

    [annee]
    widget:label = Année
    widget:atts:maxlength = 4
    widget:type = select
    vocabulary = Années

    [public]
    validators:required = true 
    widget:type = select
    vocabulary = Publics


onglets
--------

relationships.ini
~~~~~~~~~~~~~~~~~~

On souhaite inscrire des participants à une formation.

.. code-block:: sql

    DROP   TABLE IF EXISTS `individu`;
    CREATE TABLE `individu` (
      `id`              int(11)     NOT NULL AUTO_INCREMENT,
      `nom`             varchar(45) NOT NULL,
      `prenom`          varchar(45),
      `civilite`        varchar(3),
      `id_formation`    int(11),
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


.. note:: Ici, une formation peut avoir plusieurs participants mais un participant ne peut pas suivre plusieurs formations (cardinalités n-1).
          Voir plus loin pour une relation n-m.

On complètera le fichier *relationships.ini* comme suit

.. code-block:: ini

    [Participants]
    action:label = Participants
    individu.id_formation = "$id"
    metafields:order = nom ASC, prenom ASC


.. note:: Exercice

          - corrigez l'étiquette du champ *civilite* pour l'entité *individu*
          - cachez l'id (de l'individu) dans le listing des *individus*
          - corrigez l'étiquette du champ *annee* pour l'entité *période*


tests du site
--------------

urls de connexion
~~~~~~~~~~~~~~~~~~

Selon votre configuration du serveur Web

* htdocs/xata_tuto : http://127.0.0.1/xata_tuto
* www/xata_tuto : http://127.0.0.1/xata_tuto
* public_html/xata_tuto :  http://127.0.0.1/~$USER/xata_tuto


.. note:: Exercice pour aller plus loin : dans la table *individu*

          - Supprimez le champ *id_formation*
          - Créez un champ *role* avec un vocabulaire statique : participant / formateur

          Puis créez 2 relations n-m depuis l'entité *formation*
          
          - formation -> participants
          - individu -> formateurs

.. tip:: inspirez-vous de l'exemple du tutoriel officiel : http://www.xataface.com/documentation/tutorial/getting_started/relationships


ressources documentaires
-------------------------

*kit de démarrage*

- http://www.xataface.com/documentation/tutorial/getting_started/customizing
- http://xataface.com/wiki/fields.ini_file
- https://github.com/shannah/xataface/blob/master/docs/FieldsIniFile.md

*widgets disponibles*

- http://xataface.com/wiki/widget%3Atype
- https://github.com/shannah/xataface/blob/master/docs/Widgets.md

*attributs CSS*

- mode saisie : http://xataface.com/wiki/widget%3Aatts
- mode consultation : http://xataface.com/forum/viewtopic.php?t=5217

*valuelists.ini*

- https://github.com/shannah/xataface/blob/master/docs/Valuelists.md

*relationships.ini*

- http://xataface.com/wiki/relationships.ini_file
- http://xataface.com/wiki/index.php?-table=wiki&-action=view&-cursor=78&-skip=60&-limit=30&-mode=list
