sécurité (~ 5 min) / à tester + valider
========================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------

.. tip:: Ouvrez un terminal et déplacez-vous jusqu'au répertoire de votre serveur Web

         WEBSITE=$PWD


niveau serveur
---------------

principes
~~~~~~~~~~

Il est possible de placer les identifiants de connexion dans un autre fichier que *conf.ini*.


conf.db.ini
~~~~~~~~~~~~

Créez un fichier *conf.db.ini* un répertoire au-dessus de *conf.ini*

.. code-block:: bash

    cd $WEBSITE
    touch conf.db.ini

et déplacez la rubrique *[_database]* de *conf.ini* vers ce nouveau fichier.

Ensuite, inscrivez au début du fichier *conf.ini*

.. code-block:: ini

    __include__="../conf.db.ini"


.. note:: Vous positionnerez les droits en lecture seule pour l'utilisateur *www-data*.

.. tip:: Ce procédé simplifie la migration de l'application d'un environnement à un autre sans écraser les identifiants BD propres à l'environnement cible.


architecture cible
~~~~~~~~~~~~~~~~~~~

::

    WEBSITE
       |
       + conf.db.ini
       + xataface/ = __DATAFACE_PATH__
       + xata_tuto/ = __DATAFACE_SITE_PATH__
             |
             + conf.ini
             + index.php
             + .htaccess (fichier caché)
            ...


.. note:: Exercice : déplacez le fichier *conf.db.ini* en dehors du répertoire *htdocs*, *www* ou *htdocs* selon votre configuration.


utilisateurs authentifiés
--------------------------

table des utilisateurs
~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: sql

    DROP TABLE IF EXISTS `utilisateur`;
    CREATE TABLE `utilisateur` (
      `id` int(11)           NOT NULL AUTO_INCREMENT,
      `username` varchar(45) NOT NULL,
      `password` varchar(45) NOT NULL,
      `role` varchar(45)     NOT NULL DEFAULT 'READ ONLY',
      PRIMARY KEY (`id`),
      UNIQUE KEY `username_UNIQUE` (`username`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    INSERT INTO `utilisateur`
         VALUES (1,'admin',   md5('admin'),'ADMIN'),
                (2,'readonly',md5('readonly'), 'READ ONLY'),
                (3,'edit',    md5('edit'),     'EDIT'),
                (4,'manager', md5('manager'),  'MANAGER');


On ajouter les fichiers de paramétrage *fields.ini* et *valuelist.ini* pour cette nouvelle table

.. code-block:: bash

    table=utilisateur

    cd $WEBSITE
    mkdir -p xata_tuto/tables/$table
    cd xata_tuto/tables/$table
    touch fields.ini valuelists.ini


architecture cible
~~~~~~~~~~~~~~~~~~~

::

    WEBSITE
       |
       + xataface/ = __DATAFACE_PATH__
       + xata_tuto/ = __DATAFACE_SITE_PATH__
              |
              + conf.ini
              + .htaccess (fichier caché)
              + index.php
              + tables/
                   |
                   + utilisateur/
                        |
                        + fields.ini
                        + valuelists.ini
                   |
                   + ...
              |
              + ...


formulaire
~~~~~~~~~~~

Construisez le fichier *fields.ini* en paramétrant les règles suivantes :

**fields.ini**

.. code-block:: ini

    ;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -| CONFORT

    [id]
    visibility:list = hidden
    visibility:browse = hidden
    visibility:find = hidden


    ;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - | IDENTITÉ

    [username]
    widget:label = "Nom de l'utilisateur"

    [password]
    widget:label = "Mot de passe"
    ;; le paramètre encryption est valide pour toute variable
    ;; comportant le texte "password" ou "Password"
    encryption = md5
    widget:atts:class=passwordTwice

    [role]
    widget:label = Rôle
    widget:type = select
    validators:required = true
    vocabulary = Roles


Remplissez le fichier *valuelist.ini* comme suit :

**valuelist.ini**

.. code-block:: ini

    [Roles]
    'READ ONLY' = "READ ONLY"
    EDIT = EDIT
    DELETE = DELETE
    OWNER = OWNER
    REVIEWER = REVIEWER
    USER = USER
    ADMIN = ADMIN
    MANAGER = MANAGER
    'NO ACCESS' = "NO ACCESS"


activer l'authentification
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: php

    <?php
    class conf_ApplicationDelegate {
        // Hook called before the request....

        function getPermissions(&$record){
            // $record is a Dataface_Record object
            $auth =& Dataface_AuthenticationTool::getInstance();
            $user =& $auth->getLoggedInUser();
    
            if ( ! isset($user) ) return Dataface_PermissionsTool::NO_ACCESS();
            else {
                $role = $user->val('role');
                return Dataface_PermissionsTool::getRolePermissions($role);
                // Returns all of the permissions for the user's current role.
            }

        }


protéger la table des utilisateurs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**conf.ini**

.. code-block:: ini

    [_disallowed_tables]
    r1 = utilisateur


architecture cible
~~~~~~~~~~~~~~~~~~~

::

    WEBSITE
       |
       + conf.db.ini
       + xataface/ = __DATAFACE_PATH__
       + xata_tuto/ = __DATAFACE_SITE_PATH__
             |
             + conf/
                 |
                 + ApplicationDelegate.php
             |
             + conf.ini
             + index.php
             + .htaccess (fichier caché)
            ...


test du site
-------------


urls de connexion
~~~~~~~~~~~~~~~~~~

Selon votre configuration du serveur Web

* htdocs/xata_tuto : http://127.0.0.1/xata_tuto
* www/xata_tuto : http://127.0.0.1/xata_tuto
* public_html/xata_tuto :  http://127.0.0.1/~$USER/xata_tuto


ressources documentaires
-------------------------

*déporter une partie de la configuration*

- https://groups.google.com/forum/#!msg/xataface/ia5qEF1X9eE/vuTch7a4M2IJ

*champ mot de passe*

- http://xataface.com/wiki/fields.ini_file

*gestion des droits*

- http://www.xataface.com/documentation/tutorial/getting_started/permissions
- http://www.xataface.com/wiki/Application_Delegate_Class
- http://xataface.com/forum/viewtopic.php?t=4661
- http://xataface.com/documentation/how-to/disallow_tables
