installation (~ 10 min)
========================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


pré-requis
-----------

environnement
~~~~~~~~~~~~~~

* serveur LAMP

.. warning:: Les règles de sécurité sont données pour Apache au format *.htaccess*.

             Si vous n'utilisez pas Apache, adaptez-les à votre environnement.

             Sur un serveur Windows (IIS), adaptez la syntaxe des chemins et conservez le fichier d'exemple *Web.config*.


convention pour les exemples
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On notera **$WEBSITE** le chemin du répertoire de votre serveur WEB

* */var/www/*
* *$HOME/public_html/*
* *.../htdocs*
* ou autre selon votre configuration du serveur Web
          
.. tip:: Ouvrez un terminal et déplacez-vous jusqu'au répertoire de votre serveur Web

         WEBSITE=$PWD


installation de Xataface
-------------------------

Xataface s'installe de plusieurs façons. Le tutoriel décrit l'installation manuelle.


dernière version (dév)
~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: bash

    cd $WEBSITE
    ## on télécharge la version courante disponible
    ## sur Github https://github.com/shannah/xataface/tree/master
    git clone https://github.com/shannah/xataface.git

.. note:: Vous pouvez également utiliser la dernière version stable disponible sur https://github.com/shannah/xataface/releases.


Le répertoire *xataface/site_skeleton* contient 4 fichiers modèles :

* .htaccess, pour un serveur Apache, fichier caché
* conf.ini
* index.php
* Web.config, pour un serveur IIS

.. code-block:: bash

    cd $WEBSITE
    cp -R xataface/site_skeleton xata_tuto
    rm xata_tuto/Web.config


architecture cible
~~~~~~~~~~~~~~~~~~~

::

    WEBSITE
      |
      + xataface/ = __DATAFACE_PATH__
      + xata_tuto/ = __DATAFACE_SITE_PATH__
             |
             + conf.ini
             + .htaccess (fichier caché)
             + index.php


initialisation
---------------

base de données
~~~~~~~~~~~~~~~~

Initialisez votre base de données avec le script ci-dessous.

.. code-block:: sql

	DROP   TABLE IF EXISTS `formation`;
	CREATE TABLE `formation` (
	  `id`              int(11)     NOT NULL AUTO_INCREMENT,
	  `titre`           varchar(45) NOT NULL,
	  `annee`           varchar(4),
	  `public`          varchar(45),
	  `cloture`         date      ,
	  `lieu_formation`  text,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

.. note:: À l'exception des champs DATE, l'attribut NOT NULL d'un champ le rendra automatiquement obligatoire à la saisie.


conventions Xataface
~~~~~~~~~~~~~~~~~~~~~

Par défaut, [#f1]_

* le premier champ varchar de la table sera utilisé comme *titre* de l'enregistrement ; dans notre cas, il s'agit de la colonne *titre*
* l'écran de consultation/saisie affiche les champs dans l'ordre de la table
* les noms des tables seront repris dans les textes : *créer un(e) formation*, *rechercher des formations*
* les étiquettes des champs à saisir reprennent le nom du champ en convertissant les underscores par des espaces [#f2]_
  
.. rubric:: Notes

.. [#f1] Le paramétrage permettra de modifier ces comportements.
.. [#f2] Exemple : Le champ *lieu_formation* de la base de données sera intitulé *lieu formation* dans l'application.


adaptation du fichier index.php
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: php

    <?php
    // require_once '{__DATAFACE_PATH__}/public-api.php';
    // il s'agit du chemin sur disque (/.../...) vers "xataface"
    require_once '../xataface/dataface-public-api.php';

    // df_init(__FILE__, '{__DATAFACE_URL__}')->display();
    // il s'agit du chemin Web (http://...) pour accéder aux images et styles CSS
    df_init(__FILE__, '../xataface')->display();

.. note:: Par convention, dans les scripts PHP, la balise *<?php* n'est pas fermée.


adaptation du fichier conf.ini
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Adaptez le fichier *conf.ini* avec vos identifiants de connexion.

.. code-block:: ini

    ;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - | CONNEXION

    [_database]
    host = "{__DATABASE_HOST__}"
    user = "{__DATABASE_USER__}"
    password = "{__DATABASE_PASSWORD__}"
    name = "{__DATABASE_NAME__}"
    driver = mysqli

.. note:: Exemple pour une base de données locale sur le port 3310

    host = 127.0.0.1:3310


Ajoutez une rubrique *[_tables]* à la suite de la rubrique *[_database]* :

.. code-block:: ini

    ;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - | DATABASE

    [_tables]
    formation = formations

* à gauche = nom de la table en base de données
* à droite = texte qui apparaîtra pour l'utilisateur

.. tip:: En indiquant un pluriel, l'application adaptera le pluriel/singulier : *créer un(e) formation* / *rechercher des formations*.


fichier .htaccess
~~~~~~~~~~~~~~~~~~
 
.. code-block::

    <FilesMatch "\.ini$">
        # Apache 2.2
        <IfModule !mod_authz_core.c>
            Deny from all
        </IfModule>
    
        # Apache 2.4
        <IfModule mod_authz_core.c>
            Require all denied
        </IfModule>
    </FilesMatch>


répertoire templates_c
~~~~~~~~~~~~~~~~~~~~~~~

Répertoire des templates des pages générées à la volée

.. code-block:: bash

    cd $WEBSITE
    cd xata_tuto
    mkdir templates_c


architecture cible
~~~~~~~~~~~~~~~~~~~

::

    WEBSITE
      |
      + xataface/ = __DATAFACE_PATH__
      + xata_tuto/ = __DATAFACE_SITE_PATH__
             |
             + template_c/
             + conf.ini
             + .htaccess (fichier caché)
             + index.php


rectification des droits sur les fichiers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Positionnez les droits pour l'utilisateur *www-data* [#f4]_ comme suit

* r-x
   * répertoire *xataface* + récursif
   * répertoire *xata_tuto* + récursif
* rwx
   * répertoire *xata_tuto/templates_c*

.. rubric:: Notes

.. [#f4] Adaptez *www-data:www-data* ou *apache:apache* selon votre distribution Linux.


test du site
-------------

urls de connexion
~~~~~~~~~~~~~~~~~~

Selon votre configuration du serveur Web

* htdocs/xata_tuto : http://127.0.0.1/xata_tuto
* www/xata_tuto : http://127.0.0.1/xata_tuto
* public_html/xata_tuto :  http://127.0.0.1/~$USER/xata_tuto


erreurs fréquentes
~~~~~~~~~~~~~~~~~~~

*page blanche*

* origine : erreur de chemin ou erreur de droits utilisateur
* approche : consultez le journal apache */var/log/apache2/error.log*
* corrections possibles

  * modifiez les droits de l'utilisateur *www-data* ou *apache*
  * fichier *index.php* : modifiez le chemin à la ligne *require_once...*
  
*ni image ni style CSS*

* origine : erreur dans le chemin aux ressources statiques images et styles CSS
* correction possible : fichier *index.php* : modifiez le chemin à la ligne *df_init...*


ressources documentaires
-------------------------

- http://xataface-tips.blogspot.fr/2014/06/getting-started-most-common-mistakes.html
