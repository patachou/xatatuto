initialisation (~ 5 min)
=========================

.. contents:: Contenu
   :local:
   :backlinks: top

-------

.. tip:: Ouvrez un terminal et déplacez-vous jusqu'au répertoire de votre serveur Web

         WEBSITE=$PWD


par paramétrage des champs
---------------------------

timestamp
~~~~~~~~~~

Pour les champs *timestamp*, le paramétrage permet de définir des mises à jour sur création ou modification de l'enregistrement.

Dans ce cas, modifiez le fichier *fields.ini* comme suit et pour le champ qui vous intéresse utilisez une des deux formes suivantes


.. code-block:: ini

    timestamp = insert


.. code-block:: ini

    timestamp = update


autres variables
~~~~~~~~~~~~~~~~~

Modifiez le fichier *fields.ini* de la table *formation*

Complétez pour le champ *[annee]*

.. code-block:: ini
    :emphasize-lines: 2

    [annee]
    Default = 2016


initialiser des champs via l'API
---------------------------------

Il est possible de définir des valeurs par défaut en fonction d'autres variables de contexte comme l'utilisateur, la fiche courante, l'entité parent...


principes
~~~~~~~~~~

Pour cela créez un fichier *php* portant le nom de la table (ici : *formation.php*)

.. code-block:: bash

    cd $WEBSITE
    cd xata_tuto
    cd tables
    cd formation
    touch formation.php

.. note:: Vous positionnerez les droits en lecture seule pour l'utilisateur *www-data*.

Ce fichier contiendra toutes les fonctionnalités propres à cette entité


architecture cible
~~~~~~~~~~~~~~~~~~~

::

    WEBSITE
       |
       + xataface/ = __DATAFACE_PATH__
       + xata_tuto/ = __DATAFACE_SITE_PATH__
              |
              + actions.ini
              + conf.ini
              + .htaccess (fichier caché)
              + index.php
              + tables/
                  |
                  + formation/
                       |
                       + fields.ini
                       + formation.php
                       + relationships.ini
                       + valuelists.ini
              |
              + template_c/


formation.php
~~~~~~~~~~~~~~

.. code-block:: php

    <?php
    class tables_formation {
    
        function annee__default(){
            return "2016";
        }
        
    }

.. note:: Par convention, dans les scripts PHP, la balise *<?php* n'est pas fermée.


variante par trigger
---------------------

Une autre façon de procéder est de forcer la valeur juste avant l'insertion en base de données.

Dans la classe *tables_formation*, vous définirez la fonction *beforeInsert* comme suit

.. code-block:: php


    <?php
    class tables_formation {

        function beforeInsert(Dataface_Record $record){
             $record->setValue('annee',"2017");
        }


ordre des listes d'enregistrements (par paramétrage)
-----------------------------------------------------

Pour que la table des formations s'ouvre par défaut de la plus récente fiche à la plus ancienne (selon l'id) et par ordre alphabétique, 
ajoutez en haut du fichier *fields.ini* de la table **formation**

.. code-block:: ini

    default_sort = titre, id desc


ordre des listes d'enregistrements (par code)
----------------------------------------------

Pour les anciennes versions de Xataface, il est possible d'obtenir un comportement similaire en intégrant dans le fichier *index.php* toutes les règles **avant** *require_once*

.. code-block:: php

    <?php
    
    if ( !isset($_REQUEST['-sort']) and @$_REQUEST['-table'] == 'formation' ){
        $_REQUEST['-sort'] = $_GET['-sort'] = 'titre, id desc';
    } 
    
    if ( !isset($_REQUEST['-sort']) and @$_REQUEST['-table'] == 'individu' ){
        $_REQUEST['-sort'] = $_GET['-sort'] = 'nom, prenom';
    }
    

tests du site
--------------

urls de connexion
~~~~~~~~~~~~~~~~~~

Selon votre configuration du serveur Web

* htdocs/xata_tuto : http://127.0.0.1/xata_tuto
* www/xata_tuto : http://127.0.0.1/xata_tuto
* public_html/xata_tuto :  http://127.0.0.1/~$USER/xata_tuto


ressources documentaires
-------------------------

*timestamp*

* http://xataface.com/wiki/timestamp

*initialisation des variables*

* http://xataface.com/wiki/fieldname__default
* http://xataface.com/wiki/Delegate_class_methods
* http://xataface.com/documentation/tutorial/getting_started/delegate_classes

*initialisation de l'ordre des enregistrements*

* http://xataface.com/documentation/how-to/list_tab
* http://www.xataface.com/wiki/beforeHandleRequest

*trigger*

* http://xataface.com/wiki/Delegate_class_methods
* http://www.xataface.com/documentation/tutorial/getting_started/triggers
* http://xataface-tips.blogspot.fr/2014/04/detecting-when-values-are-changed.html
