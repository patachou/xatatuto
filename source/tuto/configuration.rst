configuration (~ 5 min)
========================

.. contents:: Contenu
   :local:
   :backlinks: top

-------

.. tip:: Ouvrez un terminal et déplacez-vous jusqu'au répertoire de votre serveur Web

         WEBSITE=$PWD


base de données
----------------

liste prédéfinie
~~~~~~~~~~~~~~~~~

Pour éviter de contrôler la saisie d'un utilisateur pour le champ *année* (numérique sur 4 caractères + dates dans le futur), les années sont stockées dans une table.

L'utilisateur sélectionnera ainsi l'année dans une liste prédéfinie.


.. code-block:: sql

    DROP TABLE IF EXISTS `periode`;
    CREATE TABLE `periode` (
      `annee`       varchar(4) NOT NULL,
      `commentaire` text,
      PRIMARY KEY (`annee`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    INSERT INTO `periode`
        VALUES ('2016',NULL),
               ('2017',NULL),
               ('2018',NULL);


configuration générale
-----------------------

Ajoutez au début du fichier *conf.ini*, avant *[_database]*.

.. code-block:: ini

    ;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -| APPLICATION

    ;; barre de titre du navigateur
    title = "Xata - Tuto"

    ;; encodage export
    default_oe = UTF-8

    ;; encodage import
    default_ie = UTF-8


accès à la table année
-----------------------

Juste après *[_database]*, modifiez la rubrique *[_tables]* comme suit.

.. code-block:: ini
   :emphasize-lines: 4

    ;; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - | TABLES
    
    [_tables]
    periode       = périodes
    formation     = formations

.. tip:: Les tables apparaîtront dans le bandeau supérieur dans cet ordre.

         L'application s'ouvrira par défaut sur la première table définie dans *[_tables]*.


ouverture de l'application sur une table donnée
------------------------------------------------

Initialisez la variable *default_table* dans le fichier *conf.ini* **avant** *[_database]*

.. code-block:: ini
   :emphasize-lines: 3

    ;; ici, il s'agit du nom de la table
    ;; (par opposition au nom "user-friendly" défini dans [_tables])
    default_table = formation
    
    [_database]
    

tests du site
--------------

urls de connexion
~~~~~~~~~~~~~~~~~~

Selon votre configuration du serveur Web

* htdocs/xata_tuto : http://127.0.0.1/xata_tuto
* www/xata_tuto : http://127.0.0.1/xata_tuto
* public_html/xata_tuto :  http://127.0.0.1/~$USER/xata_tuto


références documentaires
-------------------------

*kit de démarrage*

- http://xataface.com/wiki/about
- http://xataface.com/documentation/tutorial/getting_started/first_application
- http://xataface.com/wiki/How_to_build_a_PHP_MySQL_Application_with_4_lines_of_code

*configuration avancée*

- http://xataface.com/wiki/conf.ini_file
- http://xataface.com/documentation/how-to/unicode
