--
-- Table structure for table `formation`
--
DROP   TABLE IF EXISTS `formation`;
CREATE TABLE `formation` (
  `id`      int(11)     NOT NULL AUTO_INCREMENT,
  `titre`   varchar(45) NOT NULL,
  `annee`   varchar(4)  DEFAULT NULL,
  `public`  varchar(45) DEFAULT NULL,
  `cloture` date        DEFAULT NULL,
  `lieu_formation` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Dumping data for table `formation`
--
LOCK TABLES `formation` WRITE;
INSERT INTO `formation` VALUES (1,'test2','2016','interne',NULL,'lmkjlkjlkj'),(2,'formation','2017','interne',NULL,'lmkjlkjlkj'),(3,'formation','2017','interne',NULL,'lmkjlkjlkj'),(4,'test_trigger_beforeinsert','2018','interne',NULL,NULL),(5,'test3','2018','mixte',NULL,NULL);
UNLOCK TABLES;


--
-- Table structure for table `individu`
--
DROP   TABLE IF EXISTS `individu`;
CREATE TABLE `individu` (
  `id`       int(11)     NOT NULL AUTO_INCREMENT,
  `nom`      varchar(45) NOT NULL,
  `prenom`   varchar(45) DEFAULT NULL,
  `civilite` varchar(3)  DEFAULT NULL,
  `id_formation` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


--
-- Dumping data for table `individu`
--
LOCK TABLES `individu` WRITE;
INSERT INTO `individu` VALUES (1,'A','x',NULL,1),(2,'B','x',NULL,1),(3,'C','x',NULL,1),(4,'A',NULL,NULL,1),(5,'B',NULL,NULL,1),(6,'C',NULL,NULL,1);
UNLOCK TABLES;


--
-- Table structure for table `periode`
--
DROP TABLE IF EXISTS `periode`;
CREATE TABLE `periode` (
  `annee` varchar(4) NOT NULL,
  `commentaire` text,
  PRIMARY KEY (`annee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Dumping data for table `periode`
--
LOCK TABLES `periode` WRITE;
INSERT INTO `periode` VALUES ('2016',NULL),('2017',NULL),('2018',NULL);
UNLOCK TABLES;

