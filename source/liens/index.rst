documentations officielles
===========================

Les ressources documentaires sont nombreuses :

**le site officiel** http://xataface.com
    - documentation originale : http://xataface.com/documentation
    - wiki : version augmentée de la documentation originale : http://xataface.com/wiki/
    - documentation disponible dans l'application : https://github.com/shannah/xataface/blob/master/docs

**la documentation de l'API et du framework**
    - https://rawgit.com/shannah/xataface/master/doc_output/html/index.html
    - http://xataface.com/assets/dataface/about

**le dépôt Git de Xataface**
    - https://github.com/shannah/xataface

**les documentations Doxygen des modules** et leurs dépôts SVN originaux 
    - http://xataface.com/dox/modules/

**les nouveaux dépôts Git** de ces mêmes modules
    - https://github.com/search?utf8=%E2%9C%93&q=xataface&type=Repositories&ref=searchresults
    - https://github.com/search?utf8=%E2%9C%93&q=xataface+module&type=Repositories&ref=searchresults

**les forums**
    - forum original (archive) : http://xataface.com/forum/#forum-archive
    - Google groups : https://groups.google.com/forum/#!forum/xataface

**les blogs**
    - blog consacré aux nouveautés Xataface : http://xataface-tips.blogspot.fr
    - blog personnel de Steve Hannah : http://sjhannah.com/blog/
    - blog consacré au développement de Xataface : http://xataface.blogspot.fr

**sur Twitter**
    - https://twitter.com/xataface

**sur Youtube**
    - http://www.youtube.com/channel/UCEUW1dkQn-q3IL6W5c5LV9g
