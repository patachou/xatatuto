Consultation de la liste des fiches
====================================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


trier la liste sur une colonne
-------------------------------

Cliquez sur le nom de la colonne.


modifier l'ordre des colonnes
------------------------------

**fields.ini**

.. code-block:: ini

    [monchamp]
        section:order = 2


filtre rapide "loupe"
---------------------

En cliquant sur l'en-tête d'une colonne, à côté du nom, un champ de recherche s'affiche en-dessous avec une loupe.


.. important:: lorsque le champ affiche un libellé mais que la variable stocke un identifiant numérique, la recherche ne fonctionne que si on recherche l'identifiant.


ajouter des champs qui fonctionnent avec les recherches "loupe"
-----------------------------------------------------------------

**objetABC.php**

.. code-block:: php

    <?php
    ...

    function __sql__() {
       return "SELECT o.*,referentiel.label as label_ref
              FROM objetABC o
              ...
              LEFT JOIN referentiel r ON o.id_referentiel = r.id";
    }

.. warning:: il est préférable d'éviter de nommer les champs *label*, *date* ou *action* ou alors de les protéger systématiquement avec ````` (exemple : ```referentiel`.`label```).

**fields.ini**

.. code-block:: ini

    [id_referentiel]
      visibility:list = hidden

    [label_ref]
      ;; nouveau champ issu du retour de la fonction __sql__()
      widget:label = Référentiel
      visibility.edit = hidden
      visibility.find = hidden


ajouter une liste déroulante pour filtrer sur une des valeurs existantes en base de données
--------------------------------------------------------------------------------------------

**fields.ini**

.. code-block:: ini

    [monChamp]
        filter = 1


créer une nouvelle colonne en combinant des colonnes ou des données d'autres tables
-------------------------------------------------------------------------------------

**fields.ini**

.. code-block:: ini

    __sql__ = "SELECT *,coalesce(concat(acronyme,' / ',nom),nom) as nom_complet FROM etablissement"
    ;; obligatoirement en première ligne du fichier

    [nom_complet]
        widget:label = "acronyme / nom"

Ainsi, il est possible de masquer les colonnes *nom* et *acronyme* de la liste et d'afficher à la place la colonne *nom_complet*.


modifier un champ directement depuis la liste des fiches
---------------------------------------------------------

Exemple : modifier la valeur du champ *validation* de la table objetABC directement depuis la liste des fiches, sans ouvrir chacune des fiches.

**fields.ini**

.. code-block :: ini

    [validation]
        widget:type = select
        vocabulary = Validations
        noLinkFromListView=1

**valuelists.ini**

.. code-block:: ini

    [Validations]
        1 = '1. à définir'
        2 = '2. nouveau'
        3 = '3. à reprendre'
        4 = '4. à rejeter'
        5 = '5. bon à importer'
    ;; logique : on passe d'un numéro à un numéro supérieur en suivant le workflow de validation
    ;; cf objetABC/objetABC.php -> validation__renderCell()


**objetABC.php**

.. code-block:: php

    <?php
    class tables_objetABC {

	    function validation__renderCell(Dataface_Record $record){
		
		    Dataface_JavascriptTool::getInstance()
		    ->import('editable_validation.js');
		
		    $options = array(
			    '1. à définir',
			    '2. nouveau',
			    '3. à reprendre',
			    '4. à rejeter',
			    '5. bon à importer',
		    );
		    // cf. objetABC/valuelists.ini -> [Validations]
		    // dans valuelists.ini, le premier élément de [Validations] = "1" mais dans $option, le premier élément est à la position 0.
		    $currVal = $record->val('validation') - 1;
		    $out = array();
		    $out[] = '<select class="status-drop-down" 
		      data-record-id="'.htmlspecialchars($record->getId()).'">';
		
		    foreach ( $options as $opt ){
		      //$selected = ($currVal === $opt) ? 'selected':'';
			    $selected = ($options[$currVal] === $opt) ? 'selected':'';
			    $out[] = '<option value="'.htmlspecialchars($opt).'" '.$selected.'>'
			    .htmlspecialchars($opt).'</option>';
		    }
		    $out[] = '</select>';
		    return implode("\n", $out);
	      }

     ...


**js.editable_validation.js**

.. code-block:: js

	//require <jquery.packed.js>
	//require <xataface/IO.js>
	(function(){
	  var $ = jQuery;
	  
	  //registerXatafaceDecorator() is like $(document).ready() except that it may also be called
	  // when a node is loaded via ajax.
	  registerXatafaceDecorator(function(node){
		$('select.status-drop-down[data-record-id]', node).change(function(){
		  var recordId = $(this).attr('data-record-id');
		  xataface.IO.update(recordId, {validation : $(this).val()}, function(res){
			if ( res && res.code == 200 ){
			//  alert('Issue updated successfully');
			} else if ( res && res.message ){
			  alert('Mise à jour en erreur: '+res.message);
			} else {
			  alert('Mise à jour en erreur. Consultez les logs pour obtenir le détail.');
			}
		  })
		});
	  });
	  
	})();


description d'un champ
----------------------

.. code-block:: ini

    [monchamp]
        column:legend = "en euros"

