Contrôles sur la saisie
=========================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


sélectionner une valeur dans une liste dynamique
------------------------------------------------

**fields.ini**

.. code-block:: ini

    [monchamp]
        widget:type = lookup
        widget:table = etablissement
        widget:filters:-sort=nom
        vocabulary = Etablissements

**valuelists.ini**

.. code-block:: ini

    [Etablissements]
        __sql__ = "SELECT id, nom FROM etablissement"


sélectionner une valeurs depuis une liste dynamique filtrée
------------------------------------------------------------

**fields.ini**

.. code-block:: ini

    [monchamp]
        widget:type = lookup
        widget:table = etablissement
        widget:filters:actif=1
        vocabulary = Etablissements


ajouter un contrôle de cohérence entre plusieurs champs
--------------------------------------------------------

Exemple, vérifier que la date de sortie est toujours accompagnée d'un motif et inversement.

**objetABC.php**

.. code-block:: php

    <?php

    ...

    function date_sortie__validate(&$record, $value, &$params){
        if( empty ($value) ) {
            $val_sortie = $record->val('motif');
            if ( ! empty($val_sortie) ){
                $params['message'] = 'Vous devez saisir une date !';
                return false;
            }
        } else {
            if ( empty($val_sortie) ){
                $params['message'] = 'Vous devez indiquer un motif ou supprimer la date !';
                return false;
            }
        }
        return true;
    }

.. tip:: on évitera de tester directement le retour de la fonction *empty($record->val('motif'))* qui provoque une erreur fatale sur certaines configuration Apache/PHP.
