Consultation de la fiche
=========================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


modifier le titre de la fiche
------------------------------

**objetABC.php**

Exemple d'affichage d'un titre dynamique en fonction d'une variable de l'entité ABC

.. code-block:: php

    <?php
    class tables_objetABC {
    
      function getTitle(Dataface_Record $record){
          if ($record->val('actif') == '1') {
              return $record->display('label');
          } else {
              return $record->display('label') . ' (inactif)';
          }	    
      }
    
    ...

.. note:: impacte les modes *édition* et *consultation* (browse).


modifier la référence/du titre de la fiche, lorsqu'elle s'affiche depuis une autre fiche
-----------------------------------------------------------------------------------------

.. todo:: modifier la référence/du titre de la fiche, lorsqu'elle s'affiche depuis une autre fiche


modifier l'affichage d'une date
--------------------------------

**fields.ini**

.. code-block:: ini

    date_format = "%F ... %T"
    ;;  équivalent à %Y-%m-%d %H:%M:%S (cf. PHP strftime)

