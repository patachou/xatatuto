Import
=======

.. contents:: Contenu
   :local:
   :backlinks: top

-------


paramétrer un import CSV
---------------------------

**affectation.php**

.. code-block:: php

    <?php
    class tables_affectation {
    
        function __import__CSV__affectation(&$data, $defaultValues=array()){
        // build an array of Dataface_Record objects that are to be inserted based
        // on the CSV file data.
        $records = array();
        
        // first split the CSV file into an array of rows.
        $rows = explode("\n", $data);
        foreach ( $rows as $row ){
            // We iterate through the rows and parse the name, ...
            // to that they can be stored in a Dataface_Record object.
            list($id,
                 $code,
                 $label,
                 $type,
                 $actif
                ) = explode(',', $row);
            $record = new Dataface_Record('affectation', array());
            
            // We insert the default values for the record.
            $record->setValues($defaultValues);
        
            // Now we add the values from the CSV file.
            $record->setValues(
                array(
                    'id'=>$id,
                    'code'=>$code,
                    'label'=>$label,
                    'type'=>$type,
                    'actif'=>$actif
                     )
                );
        
            // Now add the record to the output array.
            $records[] = $record;
        }
    
        // Now we return the array of records to be imported.
        return $records;
      }

    }


ajouter une aide pour l'utilisateur
------------------------------------

.. todo:: ajouter une aide pour l'utilisateur
