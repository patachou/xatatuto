suite (à reprendre)
====================

.. contents:: Contenu
   :local:
   :backlinks: top

-------

.. tip:: Ouvrez un terminal et déplacez-vous jusqu'au répertoire de votre serveur Web

         WEBSITE=$PWD


vues (exemple : atteindre des données de 2 tables)
http://xataface.com/forum/viewtopic.php?t=5286
http://xataface.com/documentation/how-to/views

versionning
page d'accueil

format d'un champ date : AAAA-MM-JJ
date_format = "%F"
strftime


à suivre
---------

pour les droits

http://xataface.com/forum/viewtopic.php?t=4661


* définition de champs ajoutés *grafted fields*

ajoutez dans confort... et définir actions.ini 

    [change_password > change_password]
        label = "Changer mon mot de passe"


pb avec la date
widget:format = "h:i a"



régler finement les rôles



configuration alternative
--------------------------

Pour des raisons de sécurité, il conviendrait de déplacer *xataface* hors de *htdocs* tout en restant accessible à l'utilisateur *apache* ou *www-data*.

Mais dans ce cas, les images et styles css ne seront plus disponibles.

Une solution serait de regrouper dans un répertoire *ressources* les trois répertoires :

* js
* images
* css


architecture cible
~~~~~~~~~~~~~~~~~~~

::

    /...
     + xataface/ = __DATAFACE_PATH__
     + WEBSITE
          |
          + xata_tuto/ = __DATAFACE_SITE_PATH__
                  |
                  + ressources/
                        |
                        + css
                        + images
                        + js
                  + conf.ini
                  + index.php
                  + .htaccess (fichier caché)

 
fichier index.php
~~~~~~~~~~~~~~~~~~

Modifiez les chemins comme suit.

.. code-block:: php
   :emphasize-lines: 4,8

    <?php
    // require_once '{__DATAFACE_PATH__}/public-api.php';
    // il s'agit du chemin sur disque (/.../...) vers "xataface"
    require_once '../../xataface/dataface-public-api.php';

    // df_init(__FILE__, '{__DATAFACE_URL__}')->display();
    // il s'agit du chemin Web (http://...) pour accéder aux images et styles CSS
    df_init(__FILE__, './ressources')->display();
