Sécurité
=========

.. contents:: Contenu
   :local:
   :backlinks: top

-------



corriger les chemins d'accès vers les modules
----------------------------------------------

Comme le fichier de configuration a été déplacé pour en empêcher l'accès via un navigateur, 
les chemins d'accès relatifs vers les modules doivent être corrigés.

**conf.ini**

.. code-block:: ini

    [_modules]
        modules_switch_user = ../../modules/switch_user/switch_user.php
        modules_datepicker = ../../modules/datepicker/datepicker.php


déporter le fichier de configuration
-------------------------------------

Le fichier de configuration est déplacé en dehors du répertoire htdocs.

**xataface-master/Dataface/Application.php** -> `Dataface_Application`

.. code-block:: php

    <?php
    ...

    // configuration par défaut
    // if ( is_readable(DATAFACE_SITE_PATH.'/conf.ini') ){
    // $conf = array_merge(parse_ini_file(DATAFACE_SITE_PATH.'/conf.ini', true), $conf);        
    
    // nouvelle configuration
    if ( is_readable(DATAFACE_PATH.'/conf.ini') ){        
    $conf = array_merge(parse_ini_file(DATAFACE_PATH.'/conf.ini', true), $conf);
    ...

