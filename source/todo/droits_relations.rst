Droits : relations
===================

.. contents:: Contenu
   :local:
   :backlinks: top

-------


.. note:: Sauf précision contraire, les modifications s’effectuent dans le fichier **relationships.ini** de l’entité.


interdire la création d'une relation vers un nouvel enregistrement
-------------------------------------------------------------------

.. code-block:: ini

    [relationABC]
        actions:addnew = 0


interdire la création d'une relation vers un enregistrement existant
---------------------------------------------------------------------

.. code-block:: ini

    [relationABC]
        actions:addexisting = 0


interdire la suppression d'une relation
-----------------------------------------

.. code-block:: ini

    [relationABC]
        actions:remove = 0


http://xataface.com/wiki/relationships.ini_file
https://groups.google.com/forum/#!topic/xataface/dQFAzQ_Zznw
