autres sujets à traiter
========================

.. contents:: Contenu
   :local:
   :backlinks: top

-------

en cours

* imports
* versionning
* cacher un onglet + HTML Report
* les champs calculés


* field:fieldname:max_length : max string length on relationship list view
* différence entre lookup/filter et where sql : recherche ?
* champs calculés field__fieldname vs champs greffés
* vues et tables créées par Xata
* `module`
* vocabulary:existing
* si même champ dans jointure -> pb à cause d'un select count (*)
