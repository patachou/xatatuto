snippets : navigation
======================


requête en base
----------------

usage
~~~~~~

page d'accueil *actions/dashboard.php*

.. code-block:: php

    <?php
    function handle(&$params) {
       
        $etab = df_get_records_array('etablissement', array('-sort'=>'nom asc'));
        $ind = df_get_records_array('individu', array('-sort'=>'nom asc,prenom asc'));
        df_display(array('etablissements'=>$etab,'individus'=>$ind), 'dashboard.html');

    }

page d'accueil *templates/dashboard.html*

.. code-block:: html

    <select onchange="window.location.href=this.options[this.selectedIndex].value">
        <option value="">Sélection ...</option>
        {foreach from=$etablissements item=etablissement}
          <option value="{$etablissement->getURL('-action=edit')}">
          {$etablissement->getTitle()}
          </option>
        {/foreach}
   </select>


URL
----

usage
~~~~~~

tester l'URL pour autoriser/interdire certaines actions en fonction de la table

.. code-block:: php

    <?php
    $app =& Dataface_Application::getInstance();
    $query =& $app->getQuery();
        
    if ( $query['-table'] != 'etablissement' ){
        return PEAR::raiseError('Cette action est dédié aux fiches établissements.');
    }

    $record =& $app->getRecord(); 


relations
----------

usage
~~~~~~

parcourir les fiches en relation

Exemple : naviguer dans les tutelles d'un établissement

.. important:: La relation *tutelles* doit être définie dans *relationships.ini*.


extrait
~~~~~~~~

.. code-block:: php

    <?php
    $tutelles =& $record->getRelatedRecords('Tutelles');
    foreach ($tutelles as $t){
        $t = df_get_record('etablissement', array('id'=>$t['id_tutelle']));
    // ...
    }


lien direct
------------

usage
~~~~~~

page d'accueil *dashboard.html*

Exemple : consulter la liste des paramètres dont le nom commence par *alerte*

.. note:: Correspond à la requête SQL *nom LIKE alerte%* (en HTML ``%`` est recodé en ``%25``).


extrait
~~~~~~~~

.. code-block:: html

    <a href="{$ENV.DATAFACE_SITE_HREF}?-table=parametre&nom=alerte%25"> paramètres "alertes"</a>


redirection
------------
