snippets : contexte
====================


contexte fiche courante
------------------------

usage
~~~~~~

Intialisation de champ en fonction de la fiche parente.


extrait
~~~~~~~~

.. code-block:: php

    <?php
    $app =& Dataface_Application::getInstance();
    $record =& $app->getRecord();
    $idparent =& $record->getValue('id');


.. tip:: Lors de la création d'une fiche enfant depuis une fiche parente, l'enregistrement courant correspond à la fiche parent (la fiche enfant n'est pas encore créée).

