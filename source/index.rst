Tutoriel
=========

.. contents:: Contenu
   :local:
   :backlinks: top

-------


Objectifs
----------

::

    - créer une application complexe en moins de 100 minutes
    - découvrir la plupart des fonctionnalités de Xataface



Contenu
--------

.. hlist::
   :columns: 2

   * consultation, listings, filtres, recherches
   * saisie : widget, contrôles, valeurs par défaut
   * ergonomie : blocs et onglets
   * relations entre les entités
   * internationalisation
   * permissions
   * historique des modifications
   * exports
   * modules
   * ...


Ressources documentaires
-------------------------

Les références sont indiquées en fin de chaque chapitre pour chaque fonctionnalité abordée.

Ces références sont issues des documentations officielles

.. toctree::
   :maxdepth: 1

   liens/index


------


Partie 1 : une application pour gérer des formations
-----------------------------------------------------


.. toctree::
   :maxdepth: 1
   :numbered: 2

   tuto/installation
   tuto/configuration
   tuto/base
   tuto/confort
   tuto/ergonomie
   tuto/initialisation

**correction**

* `paramétrage <_static/tuto/xata_tuto__param.tar.gz>`__
* `dump <_static/tuto/xata_tuto__dump.sql>`__


-----



Partie 2 : sécurité et permissions utilisateurs
------------------------------------------------

.. toctree::
   :maxdepth: 1
   :numbered: 2

   tuto/securite
   tuto/acces


-----


Extraits de code (à reprendre)
-------------------------------

.. toctree::
   :maxdepth: 1

   snippets/contexte
   snippets/navigation


-----

A intégrer
-----------

.. toctree::
   :maxdepth: 1

   todo/consultation_liste.rst
   todo/consultation.rst
   todo/droits_relations.rst
   todo/controle.rst
   todo/import.rst
   todo/reste.rst
   todo/securite.rst
   todo/suite.rst

